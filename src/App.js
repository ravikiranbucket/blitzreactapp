

import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Amplify, { Auth, Interactions } from 'aws-amplify';
import { ChatBot, AmplifyTheme, withAuthenticator } from 'aws-amplify-react';
import aws_exports from './aws-exports';
Amplify.configure(aws_exports);


// Imported default theme can be customized by overloading attributes
const myTheme = {
  ...AmplifyTheme,
  sectionHeader: {
    ...AmplifyTheme.sectionHeader,
    backgroundColor: '#ff6600'
  }
};


class App extends Component {


  handleComplete(err, confirmation) {
    if (err) {
      alert('Bot conversation failed')
      return;
    }

    alert('Success: ' + JSON.stringify(confirmation, null, 2));
    return 'Thank you! what would you like to do next?';
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Welcome to Blitz Application</h1>
        </header>
        <ChatBot
          title="Welcome"
          theme={myTheme}
          botName="blitzchatbot"
          welcomeMessage="Hello, you can start by saying 'Select a category' ?"
          onComplete={this.handleComplete.bind(this)}
          clearOnComplete={true}
        />
      </div>
    );
  }
}

export default withAuthenticator(App, true);
