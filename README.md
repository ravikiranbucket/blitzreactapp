
# Start Blitz App

Hello, this is the Blitz application powered by React JS. Here are some instructions on how to run the app.

## Locally

* `git clone https://ravikiranbucket@bitbucket.org/ravikiranbucket/blitzreactapp.git`
* cd blitzreactapp
* npm start

## Host using S3

* The changes are already hosted at http://blitzreact-dev-2018-11-16-hostingbucket.s3-website-us-east-1.amazonaws.com/
* To ensure the above link has your changes too , run `amplify publish` . Refer below sections on setting up amplify.

# Create AWS profile

Create an AWS profile using amplify-user-credentials.csv 
Lets call the AWS Profile blitz_amplify_user_profile

Here are instructions copied from [aws official doc](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html)

* Open  `amplify-user-credentials.csv`
* Copy Access & Secret keys.
* Open / create `~/.aws/credentials (Linux & Mac) or %USERPROFILE%\.aws\credentials (Windows)`
* Create a default section in the file as mentioned below

```
[default]
region=us-east-1
output=json
aws_access_key_id = <Copied Access Key>
aws_secret_access_key = <Copied Secret Key>

```

* If you have your aws command in your path, the following command should return something.
`aws ec2 describe-instances`
 

# Amplify

## Setup AWS Amplify
Setup & configure Amplify by following instructions mentioned [here](https://aws-amplify.github.io/docs/)

Here are some helpful amplify commands that we will use:

* "amplify status" will show you what you've added already and if it's locally configured or deployed
* "amplify <category> add" will allow you to add features like user login or a backend API
* "amplify push" will build all your local backend resources and provision it in the cloud
* "amplify publish" will build all your local backend and frontend resources (if you have hosting category added) and provision it in the cloud

# Neptune

Neptune is our Backend graph database. Here is how you can try to connect to it manually ( if you want ). Please note, this is an optional step.

<b> Note: before your proceed, please finish the steps mentioned in Create AWS Profile section

* log into the account, go to EC2 services and get the `ec2-host-name`
* ssh into EC2, using the NEPTUNE-KEYPAIR.pem using the following command `ssh -i NEPTUNE-KEYPAIR.pem ec2-user@<ec2-host-name>`
* Follow instructions mentioned in [this aws document](https://docs.aws.amazon.com/neptune/latest/userguide/quickstart.html#quickstart-graph-gremlin) to connect.
  * cd apache-tinkerpop-gremlin-console-3.3.2
  * run `./bin/gremlin.sh`
  * On the gremlin console, run `:remote connect tinkerpop.server conf/neptune-remote.yaml` . The yaml file has neptune server url.
  * Once your have connected to the server, run  `:remote console` . This will open a gremlin terminal on the Neptune DB instance.
  * A command like `g.V()` should successfully run.


## Steps to bulk upload data

### 1.Create necessary AWS profile

* Open  `neptune-user-credentials.csv`
* Copy Access & Secret keys.
* Open / create `~/.aws/credentials (Linux & Mac) or %USERPROFILE%\.aws\credentials (Windows)`
* Create a profile called neptune-user with the Access & SecreteKey's that you copied.

```
[profile neptune-user]
region=us-east-1
output=json
aws_access_key_id = <Copied Access Key>
aws_secret_access_key = <Copied Secret Key>

```

### 2.Login into EC2

* Open a new tab and run the following command

```
export AWS_DEFAULT_PROFILE=neptune-user
```

* Now, try to ssh into the ec2 instance

```
ssh -i NEPTUNE-KEYPAIR.pem ec2-user@ec2-35-175-124-232.compute-1.amazonaws.com
```

### 3.Upload data into S3 bucket.

* Make sure you have uploaded your csv files into S3.


### 4.Run the bulk upload

* If you wish to bulk upload data into Neptune, follow instructions mentioned in [this aws document](https://docs.aws.amazon.com/neptune/latest/userguide/bulk-load-data.html)

* Here is a sample command for reference:

```
curl -X POST \
    -H 'Content-Type: application/json' \
    http://neptunedbcluster-ofnpdjqedhzt.cluster-cryiyy1ygf5o.us-east-1.neptune.amazonaws.com:8182/loader -d '
    { 
      "source" : "s3://bulkloadtest/cisconcs4201", 
      "format" : "csv",  
      "iamRoleArn" : "arn:aws:iam::351746452477:role/NeptuneLoadFromS3", 
      "region" : "us-east-1", 
      "failOnError" : "FALSE"
    }'

```

### 5. Status of bulk upload

To know the status of your load job run following command:

```
curl -G 'http://neptunedbcluster-ofnpdjqedhzt.cluster-cryiyy1ygf5o.us-east-1.neptune.amazonaws.com:8182/loader/<jobId-from-previous-command-response>'

```

## Learn More about React.
You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).
To learn React, check out the [React documentation](https://reactjs.org/).
